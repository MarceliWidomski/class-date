/*
 * date.cpp
 *
 *  Created on: 21.03.2017
 *      Author: RENT
 */

#include "Date.h"

#include <iostream>
using namespace std;

Date::Date() {
	day = 1;
	month = 1;
	year = 1;
	isLeap = isInLeapYear();
}
//Date::Date():day(1), month(1), year(1){};	// initialization list
Date::Date(int nDay, int nMonth, int nYear) {
	day = nDay;
	month = nMonth;
	year = nYear;
	isLeap = isInLeapYear();
}
void Date::setDay() {
	int nDay;
	cout << "Enter day: ";
	cin >> nDay;
	day = nDay;
}
void Date::setMonth() {
	int nMonth;
	cout << "Enter month: ";
	cin >> nMonth;
	month = nMonth;

}
void Date::setYear() {
	int nYear;
	cout << "Enter year: ";
	cin >> nYear;
	year = nYear;
	isLeap = isInLeapYear();
}
bool Date::isValid() const {
	bool isDateValid;
	if (day < 1 || day > 31 || month < 1 || month > 12)
		isDateValid = 0;
	else if (day == 31
			&& (month == 4 || month == 6 || month == 9 || month == 11))
		isDateValid = 0;
	else if (month == 2 && (day > 29 || (isLeap == 0 && day > 28)))
		isDateValid = 0;
	else
		isDateValid = 1;
	return isDateValid;
}
void Date::addDays() {
	if (this->isValid()) {
		cout << "How many day would you like to add?" << endl;
		int toAdd;
		cin >> toAdd;
		day += toAdd;
		if (day > 31
				&& (month == 1 || month == 3 || month == 5 || month == 7
						|| month == 8 || month == 10 || month == 12)) {
			day -= 31;
			++month;
			if (month > 12) {
				month -= 12;
				++year;
			}
		} else if (day > 30
				&& (month == 4 || month == 6 || month == 9 || month == 11)) {
			day -= 30;
			++month;
		} else if (month == 2) {
			if (isLeap == 1 && day > 29) {
				day -= 29;
				++month;
			} else if (isLeap == 0 && day > 28) {
				day -= 28;
				++month;
			}
		}
	}
	cout << "Date::addDays. Date is invalid. This operation is not allowed. "
			<< endl;

}
void Date::addMonths() {
	if (this->isValid()) {
		cout << "How many months would you like to add?" << endl;
		int toAdd;
		cin >> toAdd;
		month += toAdd;
		if (month > 12) {
			month -= 12;
			++year;
		}
	}
	cout << "Date::addMonths. Date is invalid. This operation is not allowed. "
			<< endl;
}
void Date::addYears() {
	cout << "How many years would you like to add?" << endl;
	int toAdd;
	cin >> toAdd;
	year += toAdd;
}
int Date::daysTillTheEndOfTheMonth() {
	if (this->isValid()) {
		int daysOfMonth = daysOfTheMonth();
		int daysTillEnd;
		daysTillEnd = daysOfMonth - day;
		return daysTillEnd;
	}
	return -1;
}
bool Date::isInLeapYear() {
	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) // checks if year is leap year
		isLeap = 1;
	else
		isLeap = 0;
	return isLeap;
}
void Date::printData() const {
	cout << day << "." << month << "." << year << endl;
}
int Date::daysTillTheEndOfTheYear() {
	if (this->isValid()) {
		int daysTillTheEndOfTheYear = daysTillTheEndOfTheMonth();
//	cout << "Dni do konca miesiaca: " << daysTillTheEndOfTheYear << endl;
		int tempMonth = month;
		while (tempMonth < 12) {
			++tempMonth;
			daysTillTheEndOfTheYear += daysOfTheMonth(tempMonth);
		}
		return daysTillTheEndOfTheYear;
	} else
		return -1;
}
int Date::daysOfTheMonth(int tempMonth) const {
	if (this->isValid()) {
		if (tempMonth == 0)
			tempMonth = month;
		int daysOfTheMonth;
		if (tempMonth == 1 || tempMonth == 3 || tempMonth == 5 || tempMonth == 7
				|| tempMonth == 8 || tempMonth == 10 || tempMonth == 12) {
			daysOfTheMonth = 31;
		} else if (tempMonth == 4 || tempMonth == 6 || tempMonth == 9
				|| tempMonth == 11)
			daysOfTheMonth = 30;
		else {
			if (isLeap == 1)
				daysOfTheMonth = 29;
			else
				daysOfTheMonth = 28;
		}
		return daysOfTheMonth;
	}
	return -1;
}
bool Date::operator==(const Date& other) const { // definition of operator ==
	if (day == other.getDay() && month == other.getMonth()
			&& year == other.getYear())
		return true;
	return false;
}
bool Date::operator!=(const Date& other) const { // definition of operator !=
	return !operator==(other);
//		retrun !(*this == other); // alternative method
}
bool Date::operator>(const Date& other) const { // definition of operator >
	if (year > other.getYear())
		return true;
	else if (year == other.getYear()) {
		if (month > other.getMonth())
			return true;
		else if (month == other.getMonth())
			if (day > other.getDay())
				return true;
	}
	return false;
}
bool Date::operator>=(const Date& other) const { // definition of operator >=
	if (year > other.getYear())
		return true;
	else if (year == other.getYear()) {
		if (month > other.getMonth())
			return true;
		else if (month == other.getMonth())
			if (day >= other.getDay())
				return true;
	}
	return false;
}
bool Date::operator<(const Date& other) const { // definition of operator <
	if (year < other.getYear())
		return true;
	else if (year == other.getYear()) {
		if (month < other.getMonth())
			return true;
		else if (month == other.getMonth())
			if (day < other.getDay())
				return true;
	}
	return false;
}
bool Date::operator<=(const Date& other) const { // definition of operator <=
	if (year < other.getYear())
		return true;
	else if (year == other.getYear()) {
		if (month < other.getMonth())
			return true;
		else if (month == other.getMonth())
			if (day <= other.getDay())
				return true;
	}
	return false;
}
