//============================================================================
// Name        : data.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "Date.h"
using namespace std;

int main() {
	Date myFirst(12, 12, 2017);
	Date a(12, 12, 2018);
	cout << (myFirst == a) << endl;
	cout << (myFirst != a) << endl;
	cout << (myFirst > a) << endl;
	cout << (myFirst >= a) << endl;
	cout << (myFirst < a) << endl;
	cout << (myFirst <= a) << endl;

	myFirst.setDay();
	myFirst.setMonth();
	myFirst.setYear();

	cout << myFirst.daysTillTheEndOfTheMonth() << endl;
	cout << myFirst.daysTillTheEndOfTheYear() << endl;
	myFirst.printData();

	myFirst.addDays();
	myFirst.addMonths();
	myFirst.addYears();
	myFirst.printData();
	myFirst.isValid();


}
