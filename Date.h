/*
 * date.h
 *
 *  Created on: 21.03.2017
 *      Author: RENT
 */

#ifndef DATE_H_
#define DATE_H_

class Date {
public:
	Date();
	Date(int nDay, int nMonth, int nYear);
	int getDay() const {return day;}
	void setDay();
	int getMonth() const {return month;}
	void setMonth();
	int getYear() const {return year;}
	void setYear();
	void printData() const;
	bool isValid() const;
	void addDays();
	void addMonths();
	void addYears();
	int daysTillTheEndOfTheMonth();
	int daysTillTheEndOfTheYear();
	bool operator==(const Date& other) const;
	bool operator!=(const Date& other) const;
	bool operator>(const Date& other) const;
	bool operator>=(const Date& other) const;
	bool operator<(const Date& other) const;
	bool operator<=(const Date& other) const;

private:
	int day;
	int month;
	int year;
	bool isLeap;
	bool isInLeapYear();
	int daysOfTheMonth(int tempMonth = 0) const;


};
#endif /* DATE_H_ */
